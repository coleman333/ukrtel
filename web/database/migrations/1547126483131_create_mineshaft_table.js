module.exports = {
  "up": `
    CREATE TABLE mineshaft (
      id BIGINT UNSIGNED AUTO_INCREMENT UNIQUE,
      metadata_id BIGINT UNSIGNED,
      serial_number INTEGER, 
      longitude FLOAT ,
      latitude FLOAT ,
      created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      deleted_at TIMESTAMP NULL,          
      PRIMARY KEY(id),
      FOREIGN KEY (metadata_id) REFERENCES mineshaft_metadata(id) ON DELETE SET NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    `,
  "down": `
        DROP TABLE mineshaft;
    `
}