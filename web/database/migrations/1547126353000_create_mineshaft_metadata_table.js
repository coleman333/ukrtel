module.exports = {
    "up": `
        CREATE TABLE mineshaft_metadata (
          id BIGINT UNSIGNED AUTO_INCREMENT UNIQUE,
          from_tk VARCHAR(255),
          channel_start VARCHAR(255),
          until_tk VARCHAR(255),
          channel_end VARCHAR(255),
          type_mark VARCHAR(255),
          dmm FLOAT,
          lm INTEGER,
          title VARCHAR(255),
          coupling VARCHAR(255),
          owner VARCHAR(255),
          document TEXT,
          note VARCHAR(255),
          qr_code VARCHAR (255),
          created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
          deleted_at TIMESTAMP NULL,
          PRIMARY KEY(id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1;
    `,
    "down": `
        DROP TABLE mineshaft_metadata;
    `
}