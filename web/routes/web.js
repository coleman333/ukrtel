const express = require('express');
const Router = express.Router();

// Router.use('/vendor_code_search',require('./../app/Controllers/VendorCodeSearch/routes'));
Router.use('/mineshaft', require('./../app/Controllers/mineshaft/routes'));
Router.use('/docs', require('./../app/Controllers/Swagger/routes'));

module.exports = Router;

