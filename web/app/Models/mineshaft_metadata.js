module.exports = function (sequelize,Sequelize) {

    let mineshaftMetadataSchema = {
        id: {
            autoIncrement: true,
            unique: true,
            allowNull: false,
            primaryKey: true,
            type: Sequelize.BIGINT
        },
        from_tk: {
            type: Sequelize.STRING,
            // allowNull: false
        },
        channel_start: {
            type: Sequelize.STRING,
            // allowNull: false
        },
        until_tk: {
            type: Sequelize.STRING,
            // allowNull: false
        },
        channel_end: {
            type: Sequelize.STRING,
            // allowNull: false
        },
        type_mark:{
            type: Sequelize.STRING,
        },
        dmm:{
            type: Sequelize.FLOAT
        },
        lm:{
            type: Sequelize.INTEGER
        },
        title:{
            type: Sequelize.STRING,
        },
        coupling:{
            type: Sequelize.STRING,
        },
        owner:{
            type: Sequelize.STRING,
        },
        document:{
            type: Sequelize.STRING,
        },
        note:{
            type: Sequelize.STRING,
        },
        qr_code: {
            type: Sequelize.STRING,
        },
        created_at: {
            type: Sequelize.DATE
        },
        deleted_at: {
            type: Sequelize.DATE
        }

    };

    let ModelOptions = {
        paranoid: true,
        deletedAt: 'deleted_at',
        createdAt: 'created_at',
        freezeTableName: true,
        updatedAt: false
    };

    return sequelize.define('mineshaft_metadata',mineshaftMetadataSchema, ModelOptions);
};

