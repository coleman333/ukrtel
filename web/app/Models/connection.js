let Sequelize = require("sequelize");

const env = process.env;
const optionDB = {
    username: env.DB_USER,
    host: env.DB_HOST,
    password: env.DB_PASSWORD,
    database: env.DB_NAME,
    port: env.DB_PORT
};

if (env.ENVIRONMENT === 'testing') {
    optionDB.host = env.DB_HOST_TEST;
    optionDB.database = env.DB_NAME_TEST;
    optionDB.port = env.DB_PORT_TEST;
}

let sequelize = new Sequelize(optionDB.database, optionDB.username, optionDB.password, {
    host: optionDB.host,
    port: optionDB.port,
    dialect: 'mysql',
	// logging: false
});

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });

module.exports = {
    sequelize:sequelize,
    Sequelize:Sequelize
};