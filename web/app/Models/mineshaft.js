module.exports = function (sequelize, Sequelize) {

    let mineshaftSchema = {
        id: {
            autoIncrement: true,
            unique: true,
            allowNull: false,
            primaryKey: true,
            type: Sequelize.BIGINT
        },

        metadata_id: {
            type: Sequelize.BIGINT,
            references: {
                model: 'mineshaft_metadata',
                key: 'id'
            }
        },
        serial_number:{
            type: Sequelize.INTEGER
        },
        longitude: {
            type: Sequelize.FLOAT
        },
        latitude: {
            type: Sequelize.FLOAT
        },
        created_at: {
            type: Sequelize.DATE
        },
        deleted_at: {
            type: Sequelize.DATE
        },
    };

    let ModelOptions = {
        paranoid: true,
        deletedAt: 'deleted_at',
        createdAt: 'created_at',
        freezeTableName: true,
        updatedAt: false
    };

    const mineshaft = sequelize.define('mineshaft', mineshaftSchema, ModelOptions);


    // mineshaft.getQuantity = function (consignment) {
    //     return sequelize.query(`
    //     SELECT
    //         I.amount,
    //         I.ids,
    //         IM.title,
    //         IM.consignment,
    //         IM.material,
    //         IM.metadata
    //     FROM (
    //       SELECT
    //         i.metadata_id,
    //         COUNT(i.id) AS amount,
    //         GROUP_CONCAT(i.id) AS ids
    //       FROM mineshaft AS i
    //       LEFT JOIN mineshaft_metadata AS im ON im.id = i.metadata_id
    //       WHERE im.consignment LIKE :consignment AND i.is_marked = FALSE
    //       GROUP BY i.metadata_id
    //     ) AS I
    //     LEFT JOIN mineshaft_metadata AS IM ON IM.id = I.metadata_id;
	// 		`, {
    //         replacements: {
    //             consignment: consignment+'%'
    //         },
    //         type: sequelize.QueryTypes.SELECT
    //     })
    // };

    // mineshaft.prototype.prettify = function () {
    //     if (this.metadata) {
    //         const keysMetadata = [],
    //             valuesMetadata = [],
    //             meta = JSON.parse(this.metadata.metadata);
    //
    //         Object.keys(meta).forEach((item) => {
    //             keysMetadata.push(item);
    //             valuesMetadata.push(meta[item]);
    //         });
    //         return {
    //             id: `G${this.id}`,
    //             title: this.metadata.title,
    //             consignment: this.metadata.consignment,
    //             material: this.metadata.material,
    //             is_marked: this.is_marked,
    //             metadata: {keysMetadata, valuesMetadata},
    //             status: this.status,
    //             equipment: this.equipment ? this.equipment.prettify() : undefined,
    //             transactions: this.transactions
    //         };
    //     } else {
    //         // TODO: add more fields
    //         return {
    //             id: `G${this.id}`,
    //             is_marked: this.is_marked,
    //         };
    //     }
    // };

    return mineshaft;
};

