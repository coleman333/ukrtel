  const Joi = require('joi');

module.exports = {
  create: Joi.object().keys({
    name: Joi.string().required(),
    status_id: Joi.number().min(1)
  }),
  update: Joi.object().keys({
    name: Joi.string(),
    status_id: Joi.number().min(1)
  })
};
