const moment = require('moment');

class DefaultSevice {

    generateRandomString(length) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < length; i++) {
          text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }
    dateStringNormalization(date) {
        let dateString = {};
        if(date.years() > 0) {
            dateString.years = date.years();
        }
        if(date.months() > 0) {
          dateString.months = date.months();
        }
        if(date.days() > 0) {
          dateString.days = date.days();
        }
        if(date.hours() > 0) {
          dateString.hours = date.hours();
        }
        if(date.minutes() > 0) {
          dateString.minutes = date.minutes();
        }
        if(date.seconds() > 0) {
          dateString.seconds = date.seconds();
        }

        return dateString;
    }
}

module.exports = new DefaultSevice();
