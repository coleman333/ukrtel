const Excel = require('exceljs');
const _ = require('lodash');
const async = require('async');
const util = require('util');
const GlobalModel = require('@model/index');
const mineshaft = GlobalModel.mineshaft;
const mineshaft_Metadata = GlobalModel.mineshaft_metadata;

const itemProcessing = (item) => {
	return Object.keys(item).reduce((obj, key) => {
		if (typeof item[key] === 'string') {
			obj[key] = item[key].trim();
		} else {
			obj[key] = item[key];
		}
		return obj;
	}, {});
};

module.exports = {
	config: {
		user: process.env.MAIL_USER,
		password: process.env.MAIL_PASSWORD,
		host: process.env.MAIL_HOST,
		port: process.env.MAIL_PORT,
		tls: true,
		connTimeout: 5000,
		authTimeout: 5000,
		debug: console.log,
		tlsOptions: {rejectUnauthorized: false},
		box: "INBOX",
		search: ["UNSEEN"],
		markSeen: true,
		fetchUnreadOnStart: true,
	},
	uploadFile: async function (mail) {
		if (!mail.attachments || mail.attachments.length === 0) return;
		try {
			await util.promisify(async.each).call(
				async,
				mail.attachments,
				async file => {
					try {
						const workbook = await new Excel.Workbook();
						await workbook.xlsx.load(file.content);

						await util.promisify(async.each).call(
							async,
							workbook.worksheets,
							async function (worksheet, sheetId) {
								let transaction;
								try {
									const columns = _.compact(worksheet.getRow(1).values);
									const rows = worksheet.getSheetValues();

									const results = _.chain(rows)
										.compact()
										.tail()
										.filter(r=>!_.isEmpty(r))
										.map(row => _.zipObject(columns, _.tail(row)))
										.value();

									transaction = await GlobalModel.sequelize.transaction();
									if (worksheet.name.match(/Приходный ордер/)) {
										await util.promisify(async.each).call(
											async,
											results,
											async rowItem => {
												const item = itemProcessing(rowItem);

												const metadata = {
													title: item['Опис матеріалу'],
													consignment: item['Партія'],
													material: item['Матеріал'],
													metadata: item
												};

												if (!metadata.title) {
													return;
												}

												const meta = await mineshaft_Metadata.create(metadata, {transaction});
												const amount = item['Кількість'] || 1;

												return util.promisify(async.times).call(
													async,
													amount,
													async () => mineshaft.create({metadata_id: meta.id}, {transaction})
												);
											}
										);
									} else if (worksheet.name.match(/Единицы оборудования/)) {
										await util.promisify(async.each).call(
											async,
											results,
											async rowItem => {
												const item = itemProcessing(rowItem);
												return Equipment.create({
													name: item['Опис'],
													equipment_id: item['Единица оборудования'],
													mineshaft_id: item['Інвентарн.номер'],
													metadata: item
												}, {transaction});
											}
										);
									}

									await transaction.commit();
								} catch (e) {
									transaction && await transaction.rollback();
									console.error(e);
								}
							}
						);
					} catch (e) {
						console.log(`Error in file '${file.name}':`, e);
					}
				}
			);
		} catch (e) {
			console.error(e)
		}
	},
};


