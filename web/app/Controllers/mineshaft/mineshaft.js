const Controller = require('@controller/Controller');
const GlobalModel = require('@model/index');
const mineshaft = GlobalModel.mineshaft;
const mineshaftMetadata = GlobalModel.mineshaft_metadata;
const Schema = require('@schema/mineshaft');
const xlxsParserForTests = require('../../../tests/xlxsParserForTests');
const TimeNormalization = require('@service/DefaultService').dateStringNormalization;
const moment = require('moment');

class mineshaftController extends Controller {

    getAllByMineshaft(Request, Response, next) {
        mineshaft.findAll({include: [
                {
                    model: mineshaftMetadata,
                    as: 'metadata'
                }
                ],
            where:{
                serial_number: Request.params.serial_number
            }
            // order: [['created_at', 'DESC']]
        }).then((items) => {

            Response.send(items);
        }).catch((error) => {
            next(error);
        })
    }

    async insertCoordinates(Request, Response, next){
        let {serial_number, longitude , latitude} = Request.body;
        const res = await mineshaft.update(
            { longitude: longitude , latitude: latitude},
                { where: { serial_number: serial_number } }
        );
        Response.json({status:200,message: "coordinates added"});
    }

    async addQrToCableToTheMineshaft(Request, Response, next){
        let id_cable = Request.params.id_cable;
        let {qr_code} = Request.body;
        const mineshaftMetadataResult = await mineshaftMetadata.update({ qr_code: qr_code }, { where: { id: id_cable } });
        Response.json({status:200,message: "qr_code added"});
    }


    async addNewCableToTheMineshaft(Request, Response, next) {
        const {serial_number} = Request.params;

        let { from_tk,channel_start,until_tk,channel_end,
                type_mark,dmm,lm,title,coupling,owner,document,
                note,qr_code} =  Request.body;
        if(dmm){dmm=parseFloat(dmm)}else{dmm=0}
        if(lm){lm=parseInt(lm)}else{lm=0}
        const data = {
            from_tk: from_tk,
            channel_start: channel_start,
            until_tk: until_tk,
            channel_end: channel_end,
            type_mark: type_mark,
            dmm: dmm,
            lm: lm,
            title: title,
            coupling: coupling,
            owner: owner,
            document: document,
            note: note,
            qr_code: qr_code
        };

        try{
            const mineshaftMetadataItem = await mineshaftMetadata.create(data);
            const mineshaftItem = await mineshaft.findOne( {where: { serial_number: serial_number }});

            const res2 = await mineshaft.create({
                    serial_number: serial_number,
                    metadata_id: mineshaftMetadataItem.id,
                    longitude: mineshaftItem.longitude,
                    latitude: mineshaftItem.latitude
                });
            // Response.json({ metadata:data, metadata_id: res2.metadata_id})
            Response.status(200).send({ metadata:data, metadata_id: res2.metadata_id});

        }catch(error){
            console.error(error)
        }

    }

    getAllMineshafts(Request, Response, next) {
        mineshaft.findAll({
            group: ['serial_number']

        }).then((items) => {

            Response.send(items);
        }).catch((error) => {
            next(error);
        })
    }

    async fillNewDB(Request, Response, next){
        const xlxsParser = new xlxsParserForTests();
        const serial_number = Request.params.serial_number;
        await xlxsParser.uploadFile(serial_number);
        Response.send({message: 'Ok'});
    }

    get(Request, Response) {
        mineshaft.find({})
            .then(async (mineshaftArray) => {
            if (!mineshaftArray) {
                Response.status(404).send({success: false, message: 'Nothing founded'});
                return;
            }
            Response.send(mineshaftArray);
        }).catch((Error) => {
            Response.status(500).send({success: false, error: Error});
        })
    }

    create(Request, Response, next) {
        const {longitude, latitude } =  Request.body;
        mineshaft.create(longitude, latitude)
            .then((inv_value) => {
            Response.send(inv_value)
        }).catch(Error => {
            next(Error);
        });
    }

    async delete(Request, Response, next) {
        mineshaft.findById(Request.params.id).then((inv) => {
            if (!inv) {
                Response.status(404).send({success: false, message: 'mineshaft does not found'});
                return;
            }
            inv.destroy();
            Response.send(inv);
        }).catch((error) => {
            next(error);
        })
    }

    // update(Request, Response, next) {
    //     this.Joi.validate(Request.body, Schema.update).then((data) => {
    //         mineshaft.findById(Request.params.id)
    //             .then(async (item) => {
    //             item = Object.assign(item, data);
    //             item.save();
    //             Response.send(item);
    //         }).catch((error) => {
    //             next(error);
    //         })
    //     }).catch((invalid) => {
    //         next(invalid);
    //     });
    // }

    // getQuantity(Request, Response, next) {
    //     try {
    //         const { consignment } = Request.params;
    //         mineshaft.getQuantity(consignment).then(result => {
    //             if (result.length === 0) {
    //                 Response.status(404).send({success: false, message: 'Nothing founded by this consignment'});
    //             }
    //             const AllObjectsWithIds = result.map((i)=>{
    //                 const res = {};
    //                 const valuesMetadata = [];
    //                 const keysMetadata = [];
    //                 res.amount = i.amount;
    //                 res.ids = i.ids.split(',').map(id => `G${id}`);
    //                 res.title = i.title;
    //                 res.consignment = i.consignment;
    //                 res.material = i.material;
    //                 let meta = JSON.parse(i.metadata);
    //                 Object.keys(meta).forEach((item) => {
    //                     keysMetadata.push(item);
    //                     valuesMetadata.push(meta[item]);
    //                 });
    //                 res.keysMetadata = keysMetadata;
    //                 res.valuesMetadata = valuesMetadata;
    //                 return res;
    //             });
    //             Response.status(200).send({success: true, AllObjectsWithIds});
    //             return next();
    //         }).catch((error) => {
    //             next(error);
    //         })
    //     } catch (error) {
    //         console.log(error);
    //     }
    // }

    // async markmineshaft(Request, Response, next){
    //     const id = Request.params.id.replace(/^\D/, '');
    //     const mineshaft = await mineshaft.findByPk(id);
    //     if (!mineshaft) {
    //         Response.status(404).send({success: false, message: 'mineshaft does not found'});
    //         return next();
    //     }
    //     if (mineshaft.is_marked) {
    //         Response.status(406).send({success: false, message: 'mineshaft already marked'});
    //         return next();
    //     }
    //     mineshaft.is_marked = true;
    //     await mineshaft.save();
    //     Response.send(mineshaft.prettify());
    //     return next();
    // }

    // getServeDate(Request, Response, next) {
    //     const id = Request.params.id.slice(1);
    //     const arrayOfMilliseconds =[];
    //     Transaction.findAll({
    //         where: {
    //             inv_id: id,
    //             type_id: 1
    //         },
    //         order: [['created_at', 'DESC']]
    //     }).then((mountTrx) => {
    //         // console.log('==================>',mountTrx);
    //         if (!mountTrx) {
    //             Response.status(404).send({
    //                 success: false,
    //                 message: 'Mount transaction does not exists yet'
    //             });
    //             return;
    //         }
    //         Transaction.findAll({
    //             where: {
    //                 inv_id: id,
    //                 type_id: 2
    //             },
    //             order: [['created_at', 'DESC']]
    //         }).then((demountTrx) => {
    //             // console.log('==================>',demountTrx);
    //
    //             let lastDate;
    //             mountTrx.map((item,index)=>{
    //                 if (!demountTrx) {
    //                     lastDate = moment();
    //                 } else {
    //                     lastDate = demountTrx[index].created_at;
    //                 }
    //                 // console.log('==================>',lastDate);
    //
    //                 const firstDate = mountTrx[index].created_at;
    //                 console.log('==================>',firstDate,lastDate);
    //                 // moment.utc(lastDate.diff(firstDate))
    //                 // const differ = moment.utc(lastDate.diff(firstDate));
    //                 // let differ = lastDate.diff(firstDate);
    //                 //
    //                 console.log('==================> EEEE ');
    //                 let a = moment()+8000;
    //                 console.log('==================> EEEE ', moment(lastDate).diff(moment(firstDate)));
    //                 // const duration = moment.duration(lastDate.diff(firstDate));
    //                 // console.log('==================>',firstDate,lastDate);
    //
    //                 arrayOfMilliseconds.push(duration);
    //             });
    //             let allMilliseconds=0;
    //             arrayOfMilliseconds.map((item)=>{
    //                  moment(allMilliseconds).add(item, 'milliseconds')
    //             });
    //
    //
    //
    //             Response.send(TimeNormalization(allMilliseconds));
    //         }).catch((error) => {
    //             next(error);
    //         })
    //     }).catch((Error) => {
    //         next(Error);
    //     })
    // }

    // async getServeDate(Request, Response, next) {
    //     try {
    //         const id = Request.params.id.slice(1);
    //         const mounts = await Transaction.findAll({
    //             where: {
    //                 inv_id: id,
    //                 type_id: {
    //                     $in: [1, 2]
    //                 },
    //             },
    //             order: ['created_at']
    //         }) || [];
    //         if (mounts === []) {
    //             Response.status(404).send({
    //                 success: false,
    //                 message: 'Mount transaction does not exists yet'
    //             });
    //             return;
    //         }
    //         let time = 0;
    //         const lastIndex = mounts.length - 1;
    //         for (let i = 0; i <= lastIndex; i += 2) {
    //             const mountDate = mounts[i].created_at;
    //             let unmountDate;
    //             if (i + 1 <= lastIndex) {
    //                 unmountDate = mounts[i + 1].created_at;
    //             } else {
    //                 unmountDate = Date.now();
    //             }
    //
    //             time+=Math.abs(moment(mountDate).diff(unmountDate,'milliseconds'));
    //         }
    //         let duration = moment.duration(time);
    //         Response.json(TimeNormalization(duration));
    //
    //     }catch (e) {
    //         console.error(e);
    //         next(e)
    //     }
    // }

}

module.exports = mineshaftController;
