const express = require('express');
const Router = express.Router();
const mineshaftClass = require('@controller/mineshaft/mineshaft');
const mineshaft = new mineshaftClass();

Router.get('/mineshaft', mineshaft.getAllMineshafts.bind(mineshaft));           //get all mineshafts
Router.get('/:serial_number', mineshaft.getAllByMineshaft.bind(mineshaft));     //get all inside one mineshaft
Router.post('/:serial_number', mineshaft.fillNewDB.bind(mineshaft));            //fill db base
Router.put('/coordinates',mineshaft.insertCoordinates.bind(mineshaft));  //insert longitude and latitude
Router.put('/cable/:serial_number',mineshaft.addNewCableToTheMineshaft.bind(mineshaft));  //create new cable
Router.put('/add_qr/:id_cable',mineshaft.addQrToCableToTheMineshaft.bind(mineshaft));  //update new cable

module.exports = Router;


// :serial_number/:longitude:/:latitude
