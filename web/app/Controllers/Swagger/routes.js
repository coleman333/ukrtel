const express = require('express');
const Router = express.Router();
const swagger = require('swagger-ui-express');
const YAML = require('yamljs');
const mineshaftFile = YAML.load(__dirname + '/../mineshaft/routes.yaml');

Router.use('/mineshaft', swagger.serve, (req, res, next) => swagger.setup(mineshaftFile)(req, res, next));

module.exports = Router;

