require('module-alias/register');
require('dotenv').config();
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const ErrorHandler = require('@service/ErrorHandler');
// const MailListenerService = require('./app/Services/MailListener');
// const MailListener = require('mail-notifier');

if(process.env.DB_NAME && process.env.DB_HOST && process.env.DB_PASSWORD && process.env.DB_USER)
{
  require('@model/connection');
  require('@model/index');
}

// const mailListener = MailListener(MailListenerService.config, (...args) => {console.log(args)});

// mailListener.start();
// mailListener.on("mail", (mail)=>{ MailListenerService.uploadFile(mail);});
// mailListener.on("error", function(err){
//   console.log(err);
// });

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(require('./routes/web'));

app.use(function(err, req, res, next) {
  const Error = new ErrorHandler(err, res);
  Error.execute();
});
app.listen(process.env.PORT, function(err) {
    if(!err) {
        console.log(`App is running on ${process.env.PORT} port`)
    }
});

