const Controller = require('../app/Controllers/Controller');
const fs = require('mz/fs');
const path = require('path');
const Excel = require('exceljs');
const _ = require('lodash');
const async = require('async');
const util = require('util');
const GlobalModel = require('@model/index');
const mineshaft = GlobalModel.mineshaft;
const mineshaft_Metadata = GlobalModel.mineshaft_metadata;
// const s = require('../65102.xlxs')

const itemProcessing = (item) => {
    return Object.keys(item).reduce((obj, key) => {
        if (typeof item[key] === 'string') {
            obj[key] = item[key].trim();
        } else {
            obj[key] = item[key];
        }
        return obj;
    }, {});
};

class xlxsParserForTestsController extends Controller {

    async uploadFile(serial_number){
        try {
            const p = path.resolve(__dirname, './../33-221.xlsx');
            const xml = await fs.readFile(p);
            const workbook = await new Excel.Workbook();
            await workbook.xlsx.load(xml);
            workbook.worksheets.forEach(async (worksheet, sheetId) => {
                    try {
                        const columns = _.compact(worksheet.getRow(1).values);
                        const rows = worksheet.getSheetValues();
                        const results = _.chain(rows)
                            .compact()
                            .tail()
                            .filter(r => !_.isEmpty(r))
                            .map(row => _.zipObject(columns, _.tail(row)))
                            .value();

                        // if (worksheet.name.match(/ukr_tel_example/)) {
                            await util.promisify(async.each).call(
                                async,
                                results,
                                async rowItem => {
                                    const item = itemProcessing(rowItem);
                                    const metadata = {
                                        from_tk: item['Від ТК'],
                                        channel_start: item['Кан.'],
                                        until_tk: item['До ТК'],
                                        channel_end: item['Кан.2'],
                                        type_mark: item['Тип, марка'],
                                        dmm: item['D, мм'],
                                        lm: item['L, м'],
                                        title: item['Найменування'],
                                        coupling: item['Муфта'],
                                        owner: item['Власник'],
                                        document: item['Документ'],
                                        note: item['Примітка'],
                                    };
                                    if (!metadata.title) {
                                        return;
                                    }
                                    const meta = await mineshaft_Metadata.create(metadata);
                                    await mineshaft.create({metadata_id: meta.id , serial_number: serial_number});
                                }
                            );
                    } catch (e) {
                        console.error(e);
                    }
                }
            );
        } catch (e) {
            console.log(`Error in file`, e);
        }
    };
}
module.exports = xlxsParserForTestsController;
